package com.kyk.preprob.kakaopay.loadbalancer.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Builder
@NoArgsConstructor
public class ErrResponseDto {
    private int responseCd;
    private String message;

    public ErrResponseDto(int responseCd, String message) {
        this.responseCd = responseCd;
        this.message = message;
    }
}
