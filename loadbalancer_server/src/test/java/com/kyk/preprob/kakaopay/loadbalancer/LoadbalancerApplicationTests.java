package com.kyk.preprob.kakaopay.loadbalancer;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.kyk.preprob.kakaopay.loadbalancer.dto.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.client.RestTemplate;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ExtendWith(SpringExtension.class)
@WebAppConfiguration
class LoadbalancerApplicationTests {
    @InjectMocks
    LoadbalancerApplication application;

    @Mock
    RestTemplate rt = Mockito.mock(RestTemplate.class);

    private MockMvc mockMvc;

    @BeforeEach
    void init() {
        mockMvc = MockMvcBuilders.standaloneSetup(application)
                .build();
    }

    @Test
    void 로드밸런서_HEALTH_CHECK() throws Exception {
        //given
        given(application.healthCheck()).willReturn("8445 | heath check SUCCESS");

        //when & then
        mockMvc.perform(get("/")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string("8445 | heath check SUCCESS"));
    }

    @Test
    void 로드밸런서_SNED_테스트_성공() throws Exception {
        //given
        RequestSendDto requestSendDto = new RequestSendDto(10000, 3);
        ObjectMapper mapper = new ObjectMapper();
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String requestJson = ow.writeValueAsString(requestSendDto);

        ResponseSendDto responseDto = new ResponseSendDto(200, "뿌리기 성공", "가나다");
        ResponseEntity<ResponseSendDto> entity = new ResponseEntity<>(responseDto, HttpStatus.OK);

        given(application.send(1l, "방1", requestSendDto))
                .willReturn(entity);

        //when & then
        mockMvc.perform(post("/pay/sending/send")
                .contentType(MediaType.APPLICATION_JSON)
                .header("X-USER-ID", 1l)
                .header("X-ROOM-ID", "방1")
                .content(requestJson))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.responseCd").value(200))
                .andExpect(jsonPath("$.message").value("뿌리기 성공"))
                .andExpect(jsonPath("$.token").value("가나다"));
    }

    @Test
    void 로드밸런서_SEND_테스트_실패() throws Exception {
        //given
        RequestSendDto requestSendDto = new RequestSendDto(10000, 3);
        ObjectMapper mapper = new ObjectMapper();
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String requestJson = ow.writeValueAsString(requestSendDto);

        given(application.send(1l, "방1", requestSendDto))
                .willThrow();

        //when & then
        mockMvc.perform(post("/pay/sending/send")
                .contentType(MediaType.APPLICATION_JSON)
                .header("X-USER-ID", 1l)
                .header("X-ROOM-ID", "방1")
                .content(requestJson))
                .andExpect(status().isInternalServerError())
                .andExpect(jsonPath("$.responseCd").value(500))
                .andExpect(jsonPath("$.message").value("서버와의 연결이 끊겼습니다. 잠시만 기다려주세요."));
    }

    @Test
    void 로드밸런서_TAKE_테스트_성공() throws Exception {
        //given
        RequestTokenDto requestTokenDto = new RequestTokenDto("가나다");
        ObjectMapper mapper = new ObjectMapper();
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String requestJson = ow.writeValueAsString(requestTokenDto);

        ResponseTakeDto responseDto = new ResponseTakeDto(200, "뿌리기 성공", 1000);
        ResponseEntity<ResponseTakeDto> entity = new ResponseEntity<>(responseDto, HttpStatus.OK);

        given(application.take(1l, "방1", requestTokenDto))
                .willReturn(entity);

        //when & then
        mockMvc.perform(post("/pay/sending/take")
                .contentType(MediaType.APPLICATION_JSON)
                .header("X-USER-ID", 1l)
                .header("X-ROOM-ID", "방1")
                .content(requestJson))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.responseCd").value(200))
                .andExpect(jsonPath("$.message").value("뿌리기 성공"))
                .andExpect(jsonPath("$.receiveAmt").value(1000));
    }

    @Test
    void 로드밸런서_TAKE_테스트_실패() throws Exception {
        //given
        RequestTokenDto requestTokenDto = new RequestTokenDto("가나다");
        ObjectMapper mapper = new ObjectMapper();
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String requestJson = ow.writeValueAsString(requestTokenDto);

        given(application.take(1l, "방1", requestTokenDto))
                .willThrow();

        //when & then
        mockMvc.perform(post("/pay/sending/take")
                .contentType(MediaType.APPLICATION_JSON)
                .header("X-USER-ID", 1l)
                .header("X-ROOM-ID", "방1")
                .content(requestJson))
                .andExpect(status().isInternalServerError())
                .andExpect(jsonPath("$.responseCd").value(500))
                .andExpect(jsonPath("$.message").value("서버와의 연결이 끊겼습니다. 잠시만 기다려주세요."));
    }

    @Test
    void 로드밸런서_SEARCH_테스트_성공() throws Exception {
        //given
        ResponseSearchDto responseDto = ResponseSearchDto.builder()
                .responseCd(200)
                .message("조회 성공")
                .totalAmt(10000)
                .totalReceivedAmt(3334)
                .build();
        ResponseEntity<ResponseSearchDto> entity = new ResponseEntity<>(responseDto, HttpStatus.OK);

        given(application.search(1l, "방1", "가나다"))
                .willReturn(entity);

        //when & then
        mockMvc.perform(get("/pay/sending/search")
                .contentType(MediaType.APPLICATION_JSON)
                .header("X-USER-ID", 1l)
                .header("X-ROOM-ID", "방1")
                .queryParam("token", "가나다"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.responseCd").value(200))
                .andExpect(jsonPath("$.message").value("조회 성공"))
                .andExpect(jsonPath("$.totalAmt").value(10000))
                .andExpect(jsonPath("$.totalReceivedAmt").value(3334));
    }

    @Test
    void 로드밸런서_SEARCH_테스트_실패() throws Exception {
        //given
        given(application.search(1l, "방1", "가나다"))
                .willThrow();

        //when & then
        mockMvc.perform(get("/pay/sending/search")
                .contentType(MediaType.APPLICATION_JSON)
                .header("X-USER-ID", 1l)
                .header("X-ROOM-ID", "방1")
                .queryParam("token", "가나다"))
                .andExpect(status().isInternalServerError())
                .andExpect(jsonPath("$.responseCd").value(500))
                .andExpect(jsonPath("$.message").value("서버와의 연결이 끊겼습니다. 잠시만 기다려주세요."));
    }
}
