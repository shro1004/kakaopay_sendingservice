package com.kyk.preprob.kakaopay.demo.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.kyk.preprob.kakaopay.demo.dto.*;
import com.kyk.preprob.kakaopay.demo.service.CheckRequestService;
import com.kyk.preprob.kakaopay.demo.service.SendingDetailServiceImpl;
import com.kyk.preprob.kakaopay.demo.service.SendingServiceImpl;
import com.kyk.preprob.kakaopay.demo.service.SendingTokenServiceImpl;
import com.kyk.preprob.kakaopay.demo.util.exception.DividedByZeroException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.core.env.Environment;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ExtendWith(SpringExtension.class)
@WebAppConfiguration
class ApiControllerTest {
    @InjectMocks
    private ApiController apiController;
    @Mock
    private SendingServiceImpl sendingService;
    @Mock
    private SendingDetailServiceImpl detailService;
    @Mock
    private SendingTokenServiceImpl tokenService;
    @Mock
    private CheckRequestService checkRequestService;
    @Mock
    ExceptionController exceptionController;
    @Autowired
    private ApplicationContext context;

    private MockMvc mockMvc;

    @BeforeEach
    void init() {
        mockMvc = MockMvcBuilders.standaloneSetup(apiController)
                .setControllerAdvice(new ExceptionController())
                .build();
    }

    @Test
    void Send_Money_Test() throws Exception {
        //given
        RequestSendDto requestSendDto = new RequestSendDto(10000, 3);
        ObjectMapper mapper = new ObjectMapper();
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String requestJson = ow.writeValueAsString(requestSendDto);

        ResponseSendDto responseDto = new ResponseSendDto(200, "뿌리기 성공", "가나다");
//        Sending sending = new Sending(1l, "방1", 10000,3);

//        given(sendingService.insertSending(any(), any(), any())).willReturn(sending);
//        given(tokenService.makeToken()).wiilReturn("가나다");
        given(tokenService.insertSendingToken(any(), any())).willReturn(responseDto);

        //when & then
        mockMvc.perform(post("/pay/sending/send")
                .contentType(MediaType.APPLICATION_JSON)
                .header("X-USER-ID", 1l)
                .header("X-ROOM-ID", "방1")
                .content(requestJson))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.responseCd").value(200))
                .andExpect(jsonPath("$.message").value("뿌리기 성공"))
                .andExpect(jsonPath("$.token").value("가나다"));
    }

    @Test
    void Take_Money_Test() throws Exception {
        //given
        RequestTokenDto requestTokenDto = new RequestTokenDto("가나다");
        ObjectMapper mapper = new ObjectMapper();
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String requestJson = ow.writeValueAsString(requestTokenDto);

        ResponseTakeDto responseDto = new ResponseTakeDto(200, "받기 성공", 3334);

        given(detailService.updateReceivedSendingDetail(any(), any()))
                .willReturn(responseDto);

        //when & then
        mockMvc.perform(post("/pay/sending/take")
                .contentType(MediaType.APPLICATION_JSON)
                .header("X-USER-ID", 1l)
                .header("X-ROOM-ID", "방1")
                .content(requestJson))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.responseCd").value(200))
                .andExpect(jsonPath("$.message").value("받기 성공"))
                .andExpect(jsonPath("$.receiveAmt").value(3334));
    }

    @Test
    void Search_Sending_Test() throws Exception {
        //given
//        TakeFinishedInfoDto finishedInfoDto = new TakeFinishedInfoDto(3334, 1l);
//        List<TakeFinishedInfoDto> list = new ArrayList<>();
//        list.add(finishedInfoDto);

        ResponseSearchDto responseDto = ResponseSearchDto.builder()
                .responseCd(200)
                .message("조회 성공")
                .totalAmt(10000)
                .totalReceivedAmt(3334)
//                .receiveFinishedInfoList(list)
                .build();

        given(detailService.findDetailBySending(any()))
                .willReturn(responseDto);

        //when & then
        mockMvc.perform(get("/pay/sending/search")
                .contentType(MediaType.APPLICATION_JSON)
                .header("X-USER-ID", 1l)
                .header("X-ROOM-ID", "방1")
                .queryParam("token", "가나다"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.responseCd").value(200))
                .andExpect(jsonPath("$.message").value("조회 성공"))
                .andExpect(jsonPath("$.totalAmt").value(10000))
                .andExpect(jsonPath("$.totalReceivedAmt").value(3334));
    }

    @Test
    void Exception_Handler_Test() throws Exception {
        //given
        RequestSendDto requestSendDto = new RequestSendDto(10000, 0);
        ObjectMapper mapper = new ObjectMapper();
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String requestJson = ow.writeValueAsString(requestSendDto);

        //when
        when(apiController.sendMoney(any(), any(), any()))
                .thenThrow(new DividedByZeroException("0으로 나눌 수 없습니다."));

        //when & then
        mockMvc.perform(post("/pay/sending/send")
                .contentType(MediaType.APPLICATION_JSON)
                .header("X-USER-ID", 1l)
                .header("X-ROOM-ID", "방1")
                .content(requestJson))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.responseCd").value(400))
                .andExpect(jsonPath("$.message").value("0으로 나눌 수 없습니다."));
    }
}