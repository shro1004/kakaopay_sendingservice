package com.kyk.preprob.kakaopay.demo.service;

import com.kyk.preprob.kakaopay.demo.dao.SendingDetailRepository;
import com.kyk.preprob.kakaopay.demo.dao.UserJoinRoomRepository;
import com.kyk.preprob.kakaopay.demo.entity.Sending;
import com.kyk.preprob.kakaopay.demo.entity.SendingDetail;
import com.kyk.preprob.kakaopay.demo.entity.UserJoinRoom;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.BDDMockito.given;

@ExtendWith(MockitoExtension.class)
class CheckRequestServiceImplTest {
    @InjectMocks
    private CheckRequestServiceImpl checkRequestService;
    @Mock
    private SendingDetailRepository detailRepository;
    @Mock
    private UserJoinRoomRepository joinRoomRepository;

    @Test
    void 받기_요청_검증_성공() throws Exception {
        //given
        Long userId = 1l;
        String roomId = "방1";
        Sending sending = Sending.builder()
                .roomId("방1")
                .userId(2l)
                .totalAmt(100)
                .divisionCnt(10)
                .build();

        UserJoinRoom userJoinRoom = new UserJoinRoom();

        //mocking
        given(joinRoomRepository.findByUserIdAndRoomIdAndAndJoinYn(userId, roomId, "Y"))
                .willReturn(userJoinRoom);
        given(detailRepository.findBySendingIdAndReceiveUserId(sending.getSendingId(), userId))
                .willReturn(null);

        //when
        boolean flag = checkRequestService.checkTakeRequest(userId, roomId, sending);

        //then
        assertEquals(true, flag);
    }

    @Test
    void 받기_스스로_뿌리기_검증_실패시() {
        //given
        Long userId = 1l;
        String roomId = "방1";
        Sending sending = Sending.builder()
                .roomId("방1")
                .userId(1l)
                .totalAmt(100)
                .divisionCnt(10)
                .build();
        String message = "";

        //when
        try {
            boolean flag = checkRequestService.checkTakeRequest(userId, roomId, sending);
        } catch (Exception e) {
            message = e.getMessage();
        }

        //then
        assertEquals(message, "스스로 뿌리기한 건은 받을 수 없습니다.");
    }

    @Test
    void 받기_동일한_대화방_검증_실패시() {
        //given
        Long userId = 1l;
        String roomId = "방1";
        Sending sending = Sending.builder()
                .roomId("방22222")
                .userId(2l)
                .totalAmt(100)
                .divisionCnt(10)
                .build();
        String message = "";

        //when
        try {
            boolean flag = checkRequestService.checkTakeRequest(userId, roomId, sending);
        } catch (Exception e) {
            message = e.getMessage();
        }

        //then
        assertEquals(message, "뿌리기가 호출된 대화방이 아닙니다.");
    }

    @Test
    void 받기_대화방_미참가_유저_검증_실패시() {
        //given
        Long userId = 5l;
        String roomId = "방1";
        Sending sending = Sending.builder()
                .roomId("방1")
                .userId(2l)
                .totalAmt(100)
                .divisionCnt(10)
                .build();

        String message = "";

        //mocking
        given(joinRoomRepository.findByUserIdAndRoomIdAndAndJoinYn(userId, roomId, "Y"))
                .willReturn(null);

        //when
        try {
            boolean flag = checkRequestService.checkTakeRequest(userId, roomId, sending);
        } catch (Exception e) {
            message = e.getMessage();
        }

        //then
        assertEquals(message, "해당 뿌리기가 호출된 대화방에 속해있지 않습니다.");
    }

    @Test
    void 받기_이미_뿌리기_받은_유저_검증_실패시() {
        //given
        Long userId = 1l;
        String roomId = "방1";
        Sending sending = Sending.builder()
                .roomId("방1")
                .userId(2l)
                .totalAmt(100)
                .divisionCnt(10)
                .build();
        UserJoinRoom userJoinRoom = new UserJoinRoom();
        SendingDetail sendingDetail = SendingDetail.builder()
                .sendingId(sending.getSendingId())
                .order(1)
                .receiveAmt(10)
                .receiveUserId(userId)
                .build();

        String message = "";

        //mocking
        given(joinRoomRepository.findByUserIdAndRoomIdAndAndJoinYn(userId, roomId, "Y"))
                .willReturn(userJoinRoom);
        given(detailRepository.findBySendingIdAndReceiveUserId(sending.getSendingId(), userId))
                .willReturn(sendingDetail);

        //when
        try {
            boolean flag = checkRequestService.checkTakeRequest(userId, roomId, sending);
        } catch (Exception e) {
            message = e.getMessage();
        }

        //then
        assertEquals(message, "이미 한번 받은 뿌리기 건입니다.");
    }

    @Test
    void 조회_요청_검증_성공() throws Exception {
        //given
        Long userId = 1l;
        Sending sending = Sending.builder()
                .roomId("방1")
                .userId(1l)
                .totalAmt(100)
                .divisionCnt(10)
                .build();

        //when
        boolean flag = checkRequestService.checkSearchRequest(userId, sending);

        //then
        assertEquals(true, flag);
    }

    @Test
    void 조회_뿌린사람_검증_실패시() {
        //given
        Long userId = 1l;
        Sending sending = Sending.builder()
                .roomId("방1")
                .userId(2l)
                .totalAmt(100)
                .divisionCnt(10)
                .build();
        String message = "";

        //when
        try {
            boolean flag = checkRequestService.checkSearchRequest(userId, sending);
        } catch (Exception e) {
            message = e.getMessage();
        }

        //then
        assertEquals("뿌리기를 요청한 사람만 조회할 수 있습니다.", message);
    }
}