package com.kyk.preprob.kakaopay.demo.dto;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class ResponseSearchDtoTest {
    @Test
    void builder_and_getter_test() {
        //given
        final ResponseSearchDto responseSearchDto = ResponseSearchDto.builder()
                .responseCd(200)
                .message("test")
                .totalReceivedAmt(100)
                .totalAmt(1000)
                .build();

        //when
        final int responseCd = responseSearchDto.getResponseCd();
        final String message = responseSearchDto.getMessage();
        final int totRecvAmt = responseSearchDto.getTotalReceivedAmt();
        final int totAmt = responseSearchDto.getTotalAmt();

        //then
        assertThat(responseCd).isEqualTo(200);
        assertThat(message).isEqualTo("test");
        assertThat(totRecvAmt).isEqualTo(100);
        assertThat(totAmt).isEqualTo(1000);
    }
}