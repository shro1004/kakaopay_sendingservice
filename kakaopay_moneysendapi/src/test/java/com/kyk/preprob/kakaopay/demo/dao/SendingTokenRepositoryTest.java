package com.kyk.preprob.kakaopay.demo.dao;

import com.kyk.preprob.kakaopay.demo.entity.SendingToken;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.ActiveProfiles;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
@DataJpaTest
@ActiveProfiles("test-db")
class SendingTokenRepositoryTest {
    @Autowired
    private TestEntityManager entityManager;
    @Autowired
    private SendingTokenRepository tokenRepository;

    @Test
    void 해당_토큰이_존재하는지_확인() {
        //given
        SendingToken sendingToken = SendingToken.builder()
                .sendingId(1l)
                .token("가나다")
                .expireDtm(LocalDateTime.now().plusMinutes(10))
                .sendingRegistDtime(LocalDateTime.now())
                .build();

        entityManager.persist(sendingToken);

        //when
        Long count = tokenRepository.countByToken("가나다");

        //then
        assertEquals(1l, count);
    }

    @Test
    void 만기전_토큰_조회시() {
        //given
        SendingToken sendingToken = SendingToken.builder()
                .sendingId(1l)
                .token("가나다")
                .expireDtm(LocalDateTime.now().plusMinutes(10))
                .sendingRegistDtime(LocalDateTime.now())
                .build();
        entityManager.persist(sendingToken);

        //when
        SendingToken result = tokenRepository.findSendingTokenByTokenAndExpireDtmIsGreaterThan("가나다", LocalDateTime.now().plusMinutes(1));

        //then
        assertEquals("가나다", result.getToken());
        assertEquals(1l, result.getSendingId());
    }

    @Test
    void 만기_토큰_조회시() {
        //given
        SendingToken sendingToken = SendingToken.builder()
                .sendingId(1l)
                .token("가나다")
                .expireDtm(LocalDateTime.now().plusMinutes(10))
                .sendingRegistDtime(LocalDateTime.now())
                .build();
        entityManager.persist(sendingToken);

        //when
        SendingToken result = tokenRepository.findSendingTokenByTokenAndExpireDtmIsGreaterThan("가나다", LocalDateTime.now().plusMinutes(11));

        //then
        assertEquals(null, result);
    }

    @Test
    void 칠일_이내_토큰_조회시() {
        //given
        SendingToken sendingToken = SendingToken.builder()
                .sendingId(1l)
                .token("가나다")
                .expireDtm(LocalDateTime.now().plusMinutes(10))
                .sendingRegistDtime(LocalDateTime.now())
                .build();
        entityManager.persist(sendingToken);

        //when
        SendingToken result = tokenRepository.findSendingTokenByTokenAndSendingRegistDtimeGreaterThanEqual("가나다", LocalDateTime.now().minusWeeks(1));

        //then
        assertEquals("가나다", result.getToken());
        assertEquals(1l, result.getSendingId());
    }

    @Test
    void 칠일_이후_토큰_조회시() {
        //given
        SendingToken sendingToken = SendingToken.builder()
                .sendingId(1l)
                .token("가나다")
                .expireDtm(LocalDateTime.now().minusWeeks(2).plusMinutes(10))
                .sendingRegistDtime(LocalDateTime.now().minusWeeks(2))
                .build();
        entityManager.persist(sendingToken);

        //when
        SendingToken result = tokenRepository.findSendingTokenByTokenAndSendingRegistDtimeGreaterThanEqual("가나다", LocalDateTime.now().minusWeeks(1));

        //then
        assertEquals(null, result);
    }
}