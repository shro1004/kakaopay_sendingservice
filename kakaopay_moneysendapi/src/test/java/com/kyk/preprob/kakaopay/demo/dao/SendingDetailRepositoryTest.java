package com.kyk.preprob.kakaopay.demo.dao;

import com.kyk.preprob.kakaopay.demo.dto.TakeFinishedInfoDto;
import com.kyk.preprob.kakaopay.demo.entity.SendingDetail;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.time.LocalDateTime;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
@DataJpaTest
@ActiveProfiles("test-db")
//@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
class SendingDetailRepositoryTest {
    @Autowired
    private TestEntityManager entityManager;
    @Autowired
    SendingDetailRepository detailRepository;

    @Test
    void SendingId_ReceiveUserId_로_조회() {
        //given
        SendingDetail sendingDetail1 = SendingDetail.builder()
                .sendingId(1l)
                .order(1)
                .receiveAmt(10000)
                .receiveDtm(LocalDateTime.now())
                .receiveUserId(1l)
                .receiveYn("Y")
                .build();

        SendingDetail sendingDetail2 = SendingDetail.builder()
                .sendingId(2l)
                .order(1)
                .receiveAmt(10000)
                .receiveDtm(LocalDateTime.now())
                .receiveUserId(3l)
                .receiveYn("Y")
                .build();
        entityManager.persist(sendingDetail1);
        entityManager.persist(sendingDetail2);

        //when
        SendingDetail result = detailRepository.findBySendingIdAndReceiveUserId(1l, 1l);

        //then
        assertEquals(1l, result.getSendingId());
        assertEquals(1, result.getOrder());
        assertEquals(10000, result.getReceiveAmt());
        assertEquals(1l, result.getReceiveUserId());
        assertEquals("Y", result.getReceiveYn());
    }

    @Test
    void SendingId_로_배정받지_않은_상세_1건_조회() {
        //given
        SendingDetail sendingDetail1 = SendingDetail.builder()
                .sendingId(1l)
                .order(1)
                .receiveAmt(10000)
                .receiveDtm(LocalDateTime.now())
                .receiveUserId(1l)
                .receiveYn("Y")
                .build();

        SendingDetail sendingDetail2 = SendingDetail.builder()
                .sendingId(1l)
                .order(2)
                .receiveAmt(10000)
                .receiveYn("N")
                .build();
        entityManager.persist(sendingDetail1);
        entityManager.persist(sendingDetail2);

        //when
        SendingDetail result = detailRepository.findFirstBySendingIdAndReceiveYnOrderByOrderAsc(1l, "N");

        //then
        assertEquals(1l, result.getSendingId());
        assertEquals(2, result.getOrder());
        assertEquals(10000, result.getReceiveAmt());
        assertEquals("N", result.getReceiveYn());
    }

    @Test
    void SendingId_로_배정_완료된_상세_조회() {
        //given
        SendingDetail sendingDetail1 = SendingDetail.builder()
                .sendingId(1l)
                .order(1)
                .receiveAmt(10000)
                .receiveDtm(LocalDateTime.now())
                .receiveUserId(2l)
                .receiveYn("Y")
                .build();

        SendingDetail sendingDetail2 = SendingDetail.builder()
                .sendingId(1l)
                .order(2)
                .receiveAmt(10000)
                .receiveYn("N")
                .build();
        entityManager.persist(sendingDetail1);
        entityManager.persist(sendingDetail2);

        //when
        List<TakeFinishedInfoDto> result = detailRepository.findSendingDetailsBySendingIdOrderByOrderAsc(1l);

        //then
        assertEquals(10000, result.get(0).getReceiveAmt());
        assertEquals(2l, result.get(0).getReceiveUserId());
    }

    @Test
    void SendingId_로_배정_완료된_총금_조회() {
        //given
        SendingDetail sendingDetail1 = SendingDetail.builder()
                .sendingId(1l)
                .order(1)
                .receiveAmt(10000)
                .receiveDtm(LocalDateTime.now())
                .receiveUserId(2l)
                .receiveYn("Y")
                .build();

        SendingDetail sendingDetail2 = SendingDetail.builder()
                .sendingId(1l)
                .order(2)
                .receiveAmt(10000)
                .receiveYn("Y")
                .build();
        entityManager.persist(sendingDetail1);
        entityManager.persist(sendingDetail2);

        //when
        int totReceivedAmt = detailRepository.getSumReceiveAmtBySendingId(1l);

        //then
        assertEquals(20000, totReceivedAmt);
    }
}