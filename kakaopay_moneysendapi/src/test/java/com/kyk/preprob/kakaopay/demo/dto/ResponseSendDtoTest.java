package com.kyk.preprob.kakaopay.demo.dto;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

class ResponseSendDtoTest {
    @Test
    void builder_and_getter_test() {
        //given
        final ResponseSendDto responseSendhDto = ResponseSendDto.builder()
                .responseCd(200)
                .message("test")
                .token("ABC")
                .build();

        //when
        final int responseCd = responseSendhDto.getResponseCd();
        final String message = responseSendhDto.getMessage();
        final String token = responseSendhDto.getToken();

        //then
        assertThat(responseCd).isEqualTo(200);
        assertThat(message).isEqualTo("test");
        assertThat(token).isEqualTo("ABC");
    }
}