package com.kyk.preprob.kakaopay.demo.dto;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

class ErrResponseDtoTest {

    @Test
    void builder_and_getter_test() {
        //given
        final ErrResponseDto errResponseDto = ErrResponseDto.builder()
                .responseCd(200)
                .message("test")
                .build();

        //when
        final int responseCd = errResponseDto.getResponseCd();
        final String message = errResponseDto.getMessage();

        //then
        assertThat(responseCd).isEqualTo(200);
        assertThat(message).isEqualTo("test");
    }

}