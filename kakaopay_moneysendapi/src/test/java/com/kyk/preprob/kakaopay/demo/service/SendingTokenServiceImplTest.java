package com.kyk.preprob.kakaopay.demo.service;

import com.kyk.preprob.kakaopay.demo.dao.SendingTokenRepository;
import com.kyk.preprob.kakaopay.demo.dto.RequestTokenDto;
import com.kyk.preprob.kakaopay.demo.dto.ResponseSendDto;
import com.kyk.preprob.kakaopay.demo.entity.Sending;
import com.kyk.preprob.kakaopay.demo.entity.SendingToken;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willReturn;

@ExtendWith(MockitoExtension.class)
class SendingTokenServiceImplTest {
    @InjectMocks
    SendingTokenServiceImpl sendingTokenService;
    @Mock
    SendingTokenRepository tokenRepository;

    @Test
    void Sending_Token_Insert_성공() throws Exception {
        //given
        String token = "가나다";
        Sending sending = Sending.builder()
                .userId(1l)
                .roomId("방1")
                .totalAmt(10000)
                .divisionCnt(2)
                .build();
        SendingToken sendingToken = SendingToken.builder()
                .token(token)
                .sendingId(1l)
                .expireDtm(LocalDateTime.now().plusMinutes(10))
                .sendingRegistDtime(LocalDateTime.now())
                .sending(sending)
                .build();

        //mocking
        given(tokenRepository.save(any()))
                .willReturn(sendingToken);

        //when
        ResponseSendDto responseSendDto = sendingTokenService.insertSendingToken(token, sending);

        //then
        assertEquals(200, responseSendDto.getResponseCd());
        assertEquals("뿌리기 보내기 성공", responseSendDto.getMessage());
        assertEquals(token, responseSendDto.getToken());
    }

    @Test
    void 만료전_토큰으로_조회_성공() throws Exception {
        //given
        RequestTokenDto requestTokenDto = new RequestTokenDto("가나다");
        Sending sending = Sending.builder()
                .userId(1l)
                .roomId("방1")
                .totalAmt(10000)
                .divisionCnt(2)
                .build();
        SendingToken sendingToken = SendingToken.builder()
                .token("가나다")
                .expireDtm(LocalDateTime.now().plusMinutes(2))
                .sendingId(1l)
                .sendingRegistDtime(LocalDateTime.now())
                .sending(sending)
                .build();

        //mocking
        given(tokenRepository.findSendingTokenByTokenAndExpireDtmIsGreaterThan(any(), any(LocalDateTime.class)))
                .willReturn(sendingToken);

        //when
        Sending resultSending = sendingTokenService.findSendingByNotExpiredToken(requestTokenDto);

        //then
        assertEquals(sending.getUserId(), resultSending.getUserId());
        assertEquals(sending.getRoomId(), resultSending.getRoomId());
        assertEquals(sending.getTotalAmt(), resultSending.getTotalAmt());
        assertEquals(sending.getDivisionCnt(), resultSending.getDivisionCnt());
    }

    @Test
    void 만료된_토큰으로_조회시() {
        //given
        RequestTokenDto requestTokenDto = new RequestTokenDto("가나다");
        String message = "";

        //mocking
        given(tokenRepository.findSendingTokenByTokenAndExpireDtmIsGreaterThan(any(), any()))
                .willReturn(null);

        //when
        try {
            Sending resultSending = sendingTokenService.findSendingByNotExpiredToken(requestTokenDto);
        } catch (Exception e) {
            message = e.getMessage();
        }

        //then
        assertEquals("뿌린지 10분이 지나 만료된 건입니다.", message);
    }

    @Test
    void 칠일이내_토큰_조회_성공() throws Exception {
        //given
        String token = "가나다";
        Sending sending = Sending.builder()
                .userId(1l)
                .roomId("방1")
                .totalAmt(10000)
                .divisionCnt(2)
                .build();
        SendingToken sendingToken = SendingToken.builder()
                .token("가나다")
                .expireDtm(LocalDateTime.now().plusMinutes(2))
                .sendingId(1l)
                .sendingRegistDtime(LocalDateTime.now())
                .sending(sending)
                .build();

        //mocking
        given(tokenRepository.findSendingTokenByTokenAndSendingRegistDtimeGreaterThanEqual(any(), any(LocalDateTime.class)))
                .willReturn(sendingToken);

        //when
        Sending resultSending = sendingTokenService.findSendingByBefore7DaysDtime(token);

        //then
        assertEquals(sending.getUserId(), resultSending.getUserId());
        assertEquals(sending.getRoomId(), resultSending.getRoomId());
        assertEquals(sending.getTotalAmt(), resultSending.getTotalAmt());
        assertEquals(sending.getDivisionCnt(), resultSending.getDivisionCnt());
    }

    @Test
    void 칠일이후_토큰_조회시() {
        //given
        String token = "가나다";
        String message = "";

        //mocking
        given(tokenRepository.findSendingTokenByTokenAndSendingRegistDtimeGreaterThanEqual(any(), any()))
                .willReturn(null);

        //when
        try {
            Sending resultSending = sendingTokenService.findSendingByBefore7DaysDtime(token);
        } catch (Exception e) {
            message = e.getMessage();
        }

        //then
        assertEquals("7일 이내에 요청한 뿌리기가 존재하지 않습니다.", message);
    }

    @RepeatedTest(value = 5, name = "랜덤토큰 생성 테스트")
    void 랜덤토큰_생성_성공() throws Exception {
        //given

        //mocking
        given(tokenRepository.countByToken(any()))
                .willReturn(0l);

        //when
        String token = sendingTokenService.makeToken();

        //then
        assertEquals(3, token.length());

    }
}