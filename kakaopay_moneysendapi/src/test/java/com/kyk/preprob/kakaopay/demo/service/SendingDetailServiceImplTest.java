package com.kyk.preprob.kakaopay.demo.service;

import com.kyk.preprob.kakaopay.demo.dao.SendingDetailRepository;
import com.kyk.preprob.kakaopay.demo.dto.RequestSendDto;
import com.kyk.preprob.kakaopay.demo.dto.ResponseSearchDto;
import com.kyk.preprob.kakaopay.demo.dto.ResponseTakeDto;
import com.kyk.preprob.kakaopay.demo.dto.TakeFinishedInfoDto;
import com.kyk.preprob.kakaopay.demo.entity.Sending;
import com.kyk.preprob.kakaopay.demo.entity.SendingDetail;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;

@ExtendWith(MockitoExtension.class)
class SendingDetailServiceImplTest {
    @InjectMocks
    private SendingDetailServiceImpl sendingDetailService;
    @Mock
    private SendingDetailRepository detailRepository;

    @Test
    void Sending_Detail_Insert_성공() throws Exception {
        //given
        Sending sending = Sending.builder()
                .roomId("방1")
                .userId(1l)
                .totalAmt(10000)
                .divisionCnt(3)
                .build();

        //when & then
        assertDoesNotThrow(() -> sendingDetailService.insertSendingDetails(sending));
    }

    @Test
    void Sending_Detail_Insert_실패시() throws Exception {
        //given
        Sending sending = Sending.builder()
                .build();

        //when & then
        assertThrows(Exception.class,
                () -> sendingDetailService.insertSendingDetails(sending)
        );
    }

    @Test
    void Sending_Detail_Update_성공() throws Exception {
        //given
        Long userId = 1l;
        Sending sending = Sending.builder()
                .roomId("방1")
                .userId(2l)
                .totalAmt(10000)
                .divisionCnt(3)
                .build();
        SendingDetail beforeUpdate = SendingDetail.builder()
                .sendingId(1l)
                .order(1)
                .receiveAmt(3334)
                .receiveYn("N")
                .sending(sending)
                .build();
        ResponseTakeDto responseTakeDto = ResponseTakeDto.builder()
                .receiveAmt(3334)
                .responseCd(200)
                .message("뿌리기 받기 성공")
                .build();

        //mocking
        given(detailRepository.findFirstBySendingIdAndReceiveYnOrderByOrderAsc(sending.getSendingId(), "N"))
                .willReturn(beforeUpdate);

        //when
        ResponseTakeDto dto = sendingDetailService.updateReceivedSendingDetail(userId, sending);

        //then
        assertEquals(responseTakeDto.getReceiveAmt(), dto.getReceiveAmt());
        assertEquals(responseTakeDto.getResponseCd(), dto.getResponseCd());
        assertEquals(responseTakeDto.getMessage(), dto.getMessage());
    }

    @Test
    void update_진행전_분배_완료된_뿌리기_실패시() throws Exception {
        //given
        Long userId = 1l;
        Sending sending = Sending.builder()
                .roomId("방1")
                .userId(2l)
                .totalAmt(10000)
                .divisionCnt(3)
                .build();
        String message = "";

        //mocking
        given(detailRepository.findFirstBySendingIdAndReceiveYnOrderByOrderAsc(sending.getSendingId(), "N"))
                .willReturn(null);

        //when
        try {
            ResponseTakeDto dto = sendingDetailService.updateReceivedSendingDetail(userId, sending);
        } catch (Exception e) {
            message = e.getMessage();
        }

        //then
        assertEquals("이미 모두 분배된 뿌리기입니다.", message);
    }

    @Test
    void 뿌리기_상세_조회_성공() throws Exception {
        //given
        Sending sending = Sending.builder()
                .roomId("방1")
                .userId(2l)
                .totalAmt(10000)
                .divisionCnt(3)
                .build();
        TakeFinishedInfoDto finishedInfoDto1 = new TakeFinishedInfoDto(3333, 2l);
        TakeFinishedInfoDto finishedInfoDto2 = new TakeFinishedInfoDto(3334, 4l);
        List<TakeFinishedInfoDto> receiveFinishedList = new ArrayList<>();
        receiveFinishedList.add(finishedInfoDto1);
        receiveFinishedList.add(finishedInfoDto2);

        ResponseSearchDto responseSearchDto = ResponseSearchDto.builder()
                .responseCd(200)
                .message("뿌리기 조회 성공")
                .sendingTime(sending.getRgtDtm())
                .totalAmt(sending.getTotalAmt())
                .totalReceivedAmt(6667)
                .receiveFinishedInfoList(receiveFinishedList)
                .build();

        //mocking
        given(detailRepository.getSumReceiveAmtBySendingId(sending.getSendingId()))
                .willReturn(6667);
        given(detailRepository.findSendingDetailsBySendingIdOrderByOrderAsc(sending.getSendingId()))
                .willReturn(receiveFinishedList);

        //when
        ResponseSearchDto resultDto = sendingDetailService.findDetailBySending(sending);

        //then
        assertEquals(responseSearchDto.getSendingTime(), resultDto.getSendingTime());
        assertEquals(responseSearchDto.getTotalAmt(), resultDto.getTotalAmt());
        assertEquals(responseSearchDto.getTotalReceivedAmt(), resultDto.getTotalReceivedAmt());
        assertEquals(responseSearchDto.getReceiveFinishedInfoList().get(0).getReceiveAmt(),
                resultDto.getReceiveFinishedInfoList().get(0).getReceiveAmt());
        assertEquals(responseSearchDto.getReceiveFinishedInfoList().get(1).getReceiveUserId(),
                resultDto.getReceiveFinishedInfoList().get(1).getReceiveUserId());
    }

}