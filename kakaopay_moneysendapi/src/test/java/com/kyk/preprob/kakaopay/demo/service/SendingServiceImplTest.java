package com.kyk.preprob.kakaopay.demo.service;

import com.kyk.preprob.kakaopay.demo.dao.SendingRepository;
import com.kyk.preprob.kakaopay.demo.dto.RequestSendDto;
import com.kyk.preprob.kakaopay.demo.entity.Sending;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;

@ExtendWith(MockitoExtension.class)
class SendingServiceImplTest {
    @InjectMocks
    SendingServiceImpl sendingService;
    @Mock
    SendingRepository sendingRepository;

    @Test
    void Inset_Sending_성공() throws Exception {
        //given
        Long userId = 1l;
        String roomId = "방1";
        RequestSendDto requestSendDto = new RequestSendDto(10000, 2);
        Sending sending = Sending.builder()
                .userId(1l)
                .roomId("방1")
                .totalAmt(10000)
                .divisionCnt(2)
                .build();

        //mocking
        given(sendingRepository.save(any()))
                .willReturn(sending);

        //when
        Sending resultSending = sendingService.insertSending(userId, roomId, requestSendDto);

        //then
        assertEquals(userId, resultSending.getUserId());
        assertEquals(roomId, resultSending.getRoomId());
        assertEquals(requestSendDto.getTotalAmt(), resultSending.getTotalAmt());
        assertEquals(requestSendDto.getDivisionCnt(), resultSending.getDivisionCnt());
    }

    @Test
    void 나누기_0명_요청시() {
        //given
        Long userId = 1l;
        String roomId = "방1";
        RequestSendDto requestSendDto = new RequestSendDto(10000, 0);
        String message = "";

        //when
        try {
            sendingService.insertSending(userId, roomId, requestSendDto);
        } catch (Exception e) {
            message = e.getMessage();
        }

        //then
        assertEquals("뿌릴 대상 인원을 0명으로 할 수 없습니다.", message);
    }

}