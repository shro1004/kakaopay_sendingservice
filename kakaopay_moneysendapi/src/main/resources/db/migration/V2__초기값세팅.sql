-- 유저 초기값 등록
insert into pay_user values (1, '카카오', now(), now());
insert into pay_user values (2, '페이', now(), now());
insert into pay_user values (3, '서버', now(), now());
insert into pay_user values (4, '개발자', now(), now());

-- 채팅방 초기값 등록
insert into chat_room values ('room1', '단톡방1', now(),now());
insert into chat_room values ('room2', '단톡방2', now(),now());
insert into chat_room values ('room3', '단톡방3', now(),now());
insert into chat_room values ('room4', '단톡방4', now(),now());

-- 채팅방 접속 테이블 등록
-- insert into user_join_room values (DEFAULT, 1, 'room1', 'Y', now(),now());
insert into user_join_room values (1, 'room1', 'Y', now(),now());
insert into user_join_room values (2, 'room1', 'Y', now(),now());
insert into user_join_room values (3, 'room1', 'Y', now(),now());
insert into user_join_room values (4, 'room1', 'Y', now(),now());

insert into user_join_room values (1, 'room2', 'Y', now(),now());
insert into user_join_room values (2, 'room2', 'Y', now(),now());
insert into user_join_room values (3, 'room2', 'Y', now(),now());
insert into user_join_room values (4, 'room2', 'Y', now(),now());

insert into user_join_room values (1, 'room3', 'Y', now(),now());
insert into user_join_room values (2, 'room3', 'Y', now(),now());
insert into user_join_room values (3, 'room3', 'Y', now(),now());
insert into user_join_room values (4, 'room3', 'Y', now(),now());

insert into user_join_room values (1, 'room4', 'Y', now(),now());
insert into user_join_room values (2, 'room4', 'Y', now(),now());
insert into user_join_room values (3, 'room4', 'Y', now(),now());
insert into user_join_room values (4, 'room4', 'Y', now(),now());
