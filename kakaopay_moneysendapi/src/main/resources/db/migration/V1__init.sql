create table if not EXISTS pay_user
(user_id bigint not null,
 user_name varchar(50),
 rgt_dtm timestamp without time zone,
 udt_dtm timestamp without time zone,
 primary key (user_id));

create table if not EXISTS chat_room
(room_id varchar(50) not null,
 room_name varchar(50),
 rgt_dtm timestamp without time zone,
 udt_dtm timestamp without time zone,
 primary key (room_id));

create table if not EXISTS sending
(sending_id bigserial not null,
 user_id bigint not null,
 room_id varchar(50) not null,
 total_amt integer,
 div_cnt integer,
 rgt_dtm timestamp without time zone,
 udt_dtm timestamp without time zone,
 primary key (sending_id));

create table if not EXISTS user_join_room
(user_id bigint not null,
 room_id varchar(50) not null,
 join_yn varchar(1),
 rgt_dtm timestamp without time zone,
 udt_dtm timestamp without time zone,
 primary key (user_id, room_id));

create table if not EXISTS sending_detail
(sending_id bigint not null,
--  user_id bigint not null,
--  room_id varchar(50) not null,
 s_order integer not null,
 receive_user_id bigint,
 receive_amt integer,
 receive_yn varchar(1),
 receive_dtm timestamp without time zone,
primary  key (sending_id, s_order));


