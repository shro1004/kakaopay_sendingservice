create table if not EXISTS sending_token
(token varchar(3) not null,
 sending_id bigint not null,
 expire_dtm timestamp without time zone,
 sending_rgt_dtm timestamp without time zone,
 primary key (token));