package com.kyk.preprob.kakaopay.demo.dao;

import com.kyk.preprob.kakaopay.demo.dto.TakeFinishedInfoDto;
import com.kyk.preprob.kakaopay.demo.entity.SendingDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface SendingDetailRepository extends JpaRepository<SendingDetail, Long> {
    SendingDetail findBySendingIdAndReceiveUserId(Long sendingId, Long receiveUserId);

    SendingDetail findFirstBySendingIdAndReceiveYnOrderByOrderAsc(Long sendingId, String receiveYn);

    @Query(nativeQuery = true, name = "SendingDetail.GetReceiveAmtAndReceiveUserId")
    List<TakeFinishedInfoDto> findSendingDetailsBySendingIdOrderByOrderAsc(Long sendingId);

    @Query(nativeQuery = true, value = "" +
            "select coalesce(sum(sd.receive_amt), 0)    " +
            "  from sending_detail sd                   " +
            " where sd.sending_id = ?1                  " +
            "   and sd.receive_yn = 'Y'                 "
    )
    int getSumReceiveAmtBySendingId(Long sendingId);
}
