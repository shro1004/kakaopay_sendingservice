package com.kyk.preprob.kakaopay.demo.entity;

import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;

@Getter
@Setter
@ToString(exclude = {"user", "room"})
@Entity
@Table(name = "sending")
@NoArgsConstructor
public class Sending {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "sending_id", nullable = false)
    private Long sendingId;

    //    @Id
    @Column(name = "user_id", nullable = false)
    private Long userId;

    //    @Id
    @Column(name = "room_id", nullable = false)
    private String roomId;

    @Column(name = "total_amt")
    private int totalAmt;

    @Column(name = "div_cnt")
    private int divisionCnt;

    @CreationTimestamp
    @Column(name = "rgt_dtm", nullable = false)
    private LocalDateTime rgtDtm;

    @UpdateTimestamp
    @Column(name = "udt_dtm")
    private LocalDateTime udtDtm;

    @ManyToOne(fetch = FetchType.LAZY, optional = true)
    /** 연관관계만 맺는 역할만 하고 실제 값은 @id 컬럼을 실제 매핑에 사용 */
    @JoinColumn(name = "user_id", updatable = false, insertable = false, foreignKey = @ForeignKey(value = ConstraintMode.NO_CONSTRAINT))
    private User user;

    @ManyToOne(fetch = FetchType.LAZY, optional = true)
    /** 연관관계만 맺는 역할만 하고 실제 값은 @id 컬럼을 실제 매핑에 사용 */
    @JoinColumn(name = "room_id", updatable = false, insertable = false, foreignKey = @ForeignKey(value = ConstraintMode.NO_CONSTRAINT))
    private Room room;

    @Builder
    public Sending(Long userId, String roomId, int totalAmt, int divisionCnt) {
        this.userId = userId;
        this.roomId = roomId;
        this.totalAmt = totalAmt;
        this.divisionCnt = divisionCnt;
    }

}
