package com.kyk.preprob.kakaopay.demo.entity;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;

@Getter
@Setter
@Entity
@Table(name = "user_join_room")
@IdClass(UserJoinRoomId.class)
@NoArgsConstructor
public class UserJoinRoom {
    @Id
    @Column(name = "user_id", nullable = false)
    private Long userId;

    @Id
    @Column(name = "room_id", nullable = false)
    private String roomId;

    @Column(name = "join_yn")
    private String joinYn;

    @CreationTimestamp
    @Column(name = "rgt_dtm", nullable = false)
    private LocalDateTime rgtDtm;

    @UpdateTimestamp
    @Column(name = "udt_dtm")
    private LocalDateTime udtDtm;

    @ManyToOne(fetch = FetchType.LAZY, optional = true)
    /** 연관관계만 맺는 역할만 하고 실제 값은 @id 컬럼을 실제 매핑에 사용 */
    @JoinColumn(name = "user_id", updatable = false, insertable = false, foreignKey = @ForeignKey(value = ConstraintMode.NO_CONSTRAINT))
    private User user;

    @ManyToOne(fetch = FetchType.LAZY, optional = true)
    /** 연관관계만 맺는 역할만 하고 실제 값은 @id 컬럼을 실제 매핑에 사용 */
    @JoinColumn(name = "room_id", updatable = false, insertable = false, foreignKey = @ForeignKey(value = ConstraintMode.NO_CONSTRAINT))
    private Room room;

    public UserJoinRoom(/*Long joinId, */Long userId, String roomId, String joinYn) {
//        this.joinId = joinId;
        this.userId = userId;
        this.roomId = roomId;
        this.joinYn = joinYn;
    }


}
