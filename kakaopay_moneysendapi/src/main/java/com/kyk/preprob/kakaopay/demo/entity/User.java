package com.kyk.preprob.kakaopay.demo.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Set;

@Getter
@Setter
@Entity
@Table(name = "pay_user")
@NoArgsConstructor
public class User {
    @Id
    @Column(name = "user_id", nullable = false)
    private Long userId;

    @Column(name = "user_name", nullable = false)
    private String userName;

    @CreationTimestamp
    @Column(name = "rgt_dtm", nullable = false)
    private LocalDateTime rgtDtm;

    @UpdateTimestamp
    @Column(name = "udt_dtm")
    private LocalDateTime udtDtm;


}
