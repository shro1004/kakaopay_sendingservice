package com.kyk.preprob.kakaopay.demo.service;

import com.kyk.preprob.kakaopay.demo.dto.RequestSendDto;
import com.kyk.preprob.kakaopay.demo.entity.Sending;

public interface SendingSerivce {
    Sending insertSending(Long userId, String roomId, RequestSendDto requestSendDto) throws Exception;
}
