package com.kyk.preprob.kakaopay.demo.service;

import com.kyk.preprob.kakaopay.demo.entity.Sending;

public interface CheckRequestService {
    boolean checkTakeRequest(Long userId, String roomId, Sending sending) throws Exception;

    boolean checkSearchRequest(Long userId, Sending sending) throws Exception;

}
