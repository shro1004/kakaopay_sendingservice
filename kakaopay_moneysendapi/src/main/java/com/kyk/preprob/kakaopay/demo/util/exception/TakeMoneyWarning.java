package com.kyk.preprob.kakaopay.demo.util.exception;

public class TakeMoneyWarning extends Exception {
    public TakeMoneyWarning(String msg) {
        super(msg);
    }
}
