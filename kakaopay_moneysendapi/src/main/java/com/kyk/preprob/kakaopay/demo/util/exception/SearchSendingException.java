package com.kyk.preprob.kakaopay.demo.util.exception;

public class SearchSendingException extends Exception {
    public SearchSendingException(String msg) {
        super(msg);
    }
}
