package com.kyk.preprob.kakaopay.demo.service;

import com.kyk.preprob.kakaopay.demo.dao.SendingTokenRepository;
import com.kyk.preprob.kakaopay.demo.dto.RequestTokenDto;
import com.kyk.preprob.kakaopay.demo.dto.ResponseSendDto;
import com.kyk.preprob.kakaopay.demo.entity.Sending;
import com.kyk.preprob.kakaopay.demo.entity.SendingToken;
import com.kyk.preprob.kakaopay.demo.util.exception.TokenExpireException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Random;

@Slf4j
@Service("SendingTokenService")
public class SendingTokenServiceImpl implements SendingTokenService {
    private static final int HANGEUL_BASE = 0xAC00;
    @Autowired
    SendingTokenRepository tokenRepository;

    @Override
    public ResponseSendDto insertSendingToken(String token, Sending sending) throws Exception {
        ResponseSendDto responseDto;

        LocalDateTime tokenExpireDtime = LocalDateTime.now().plusMinutes(10);  // 현재시간에 10분을 더해 만기 시간에 저장

        SendingToken sendingToken = SendingToken.builder()
                .token(token)
                .sendingId(sending.getSendingId())
                .expireDtm(tokenExpireDtime)
                .sendingRegistDtime(sending.getRgtDtm())
                .sending(sending)
                .build();
        sendingToken = tokenRepository.save(sendingToken);
        log.info("sending_token Table insert SUCCESS | {}", sendingToken);

        responseDto = ResponseSendDto.builder()
                .responseCd(200)
                .message("뿌리기 보내기 성공")
                .token(token)
                .build();
        log.debug("responseDto 생성 성공 | {}", responseDto.toString());
        return responseDto;
    }

    @Override
    public Sending findSendingByNotExpiredToken(RequestTokenDto requestTokenDto) throws Exception {
        String token = requestTokenDto.getToken();

        // 1. 토큰으로 만료되지 않은 sending 을 조회한다.
        SendingToken sendingToken = tokenRepository.findSendingTokenByTokenAndExpireDtmIsGreaterThan(token, LocalDateTime.now());
        if (sendingToken == null) {
            throw new TokenExpireException("뿌린지 10분이 지나 만료된 건입니다.");
        }
        log.debug("sending token 조회 성공 | {}", sendingToken.toString());

        // 2. sending token 객체에서 sending 객체 조회
        Sending sending = sendingToken.getSending();
        log.debug("sending 조회 성공 | {}", sending.toString());

        return sending;
    }

    @Override
    public Sending findSendingByBefore7DaysDtime(String token) throws Exception {
        // 1. 토큰으로 7일 이전의 sending token 객체 조회
        SendingToken sendingToken = tokenRepository.findSendingTokenByTokenAndSendingRegistDtimeGreaterThanEqual(token,
                LocalDateTime.now().minusWeeks(1));
        if (sendingToken == null) {
            /** 7일 이상된 토큰의 경우 재사용을 위해 삭제 */
            sendingToken = tokenRepository.findByToken(token);
            if(sendingToken != null) {
                tokenRepository.delete(sendingToken);
            }
            throw new TokenExpireException("7일 이내에 요청한 뿌리기가 존재하지 않습니다.");
        }
        log.debug("sending token 조회 성공 | {}", sendingToken.toString());

        // 2. sending token 객체에서 sending 객체 조회
        Sending sending = sendingToken.getSending();
        log.debug("sending 조회 성공 | {}", sending.toString());

        return sending;
    }

    @Override
    public String makeToken() throws Exception {

        boolean flag = true;
        String sendingToken = "-_-";
        while (flag) {
            StringBuilder buffer = new StringBuilder();
            Random random = new Random();
            for (int i = 0; i < 3; i++) {
                /**
                 *  자음 랜덤 + 모음 랜덤 + 받침 랜덤
                 *  총 경우의 수 : (19 * 21 * 28)^3 = 약 1조 3900 억
                 */
                buffer.append((char) (HANGEUL_BASE + 21 * 28 * random.nextInt(19) + 28 * random.nextInt(21) + random.nextInt(28)));

//                // 숫자 및 영문으로 3글자
//                int rIndex = random.nextInt(3);
//                switch (rIndex) {
//                    case 0:
//                        // a-z
//                        buffer.append((char) (random.nextInt(26) + 97));
//                        break;
//                    case 1:
//                        // A-Z
//                        buffer.append((char) (random.nextInt(26) + 65));
//                        break;
//                    case 2:
//                        // 0-9
//                        buffer.append((random.nextInt(10)));
//                        break;
//                }
                log.debug("토큰 생성중({}/3) | Sending Token : {}", i, buffer.toString());
            }
            sendingToken = buffer.toString();

            if (tokenRepository.countByToken(sendingToken) == 0) {
                flag = false;
            } else {
                log.info("기발급된 토큰 존재로 토큰 재생성");
            }
        }
        log.info("토큰 완성 | Sending Token : {}", sendingToken);
        return sendingToken;
    }
}
