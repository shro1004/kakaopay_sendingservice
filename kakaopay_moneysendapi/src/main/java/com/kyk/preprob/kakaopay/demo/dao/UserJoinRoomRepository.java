package com.kyk.preprob.kakaopay.demo.dao;

import com.kyk.preprob.kakaopay.demo.entity.UserJoinRoom;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserJoinRoomRepository extends JpaRepository<UserJoinRoom, Long> {
    UserJoinRoom findByUserIdAndRoomIdAndAndJoinYn(Long userId, String roomId, String joinYn);
}
