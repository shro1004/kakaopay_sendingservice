package com.kyk.preprob.kakaopay.demo.dto;

import lombok.*;

@Getter
@Setter
@ToString
@Builder
@NoArgsConstructor
public class ResponseSendDto {
    private int responseCd;
    private String message;
    private String token;

    public ResponseSendDto(int responseCd, String message, String token) {
        this.responseCd = responseCd;
        this.message = message;
        this.token = token;
    }
}
