package com.kyk.preprob.kakaopay.demo.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Set;

@Getter
@Setter
@Entity
@Table(name = "chat_room")
@NoArgsConstructor
public class Room {
    @Id
    @Column(name = "room_id", nullable = false)
    private String roomId;

    @Column(name = "room_name", nullable = false)
    private String roomName;

    @CreationTimestamp
    @Column(name = "rgt_dtm", nullable = false)
    private LocalDateTime rgtDtm;

    @UpdateTimestamp
    @Column(name = "udt_dtm")
    private LocalDateTime udtDtm;


}
