package com.kyk.preprob.kakaopay.demo.util.exception;

public class DividedByZeroException extends Exception {
    public DividedByZeroException(String msg) {
        super(msg);
    }
}
