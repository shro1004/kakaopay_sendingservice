package com.kyk.preprob.kakaopay.demo.service;

import com.kyk.preprob.kakaopay.demo.dao.SendingDetailRepository;
import com.kyk.preprob.kakaopay.demo.dto.RequestSendDto;
import com.kyk.preprob.kakaopay.demo.dto.ResponseSearchDto;
import com.kyk.preprob.kakaopay.demo.dto.ResponseTakeDto;
import com.kyk.preprob.kakaopay.demo.dto.TakeFinishedInfoDto;
import com.kyk.preprob.kakaopay.demo.entity.Sending;
import com.kyk.preprob.kakaopay.demo.entity.SendingDetail;
import com.kyk.preprob.kakaopay.demo.util.exception.DividedByZeroException;
import com.kyk.preprob.kakaopay.demo.util.exception.TakeMoneyWarning;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service("SendingDetail")
public class SendingDetailServiceImpl implements SendingDetailService {
    @Autowired
    SendingDetailRepository detailRepository;

    @Override
    public void insertSendingDetails(Sending sending) throws Exception {
        int totalAmt = sending.getTotalAmt();
        int divCnt = sending.getDivisionCnt();

        int divAmt = totalAmt / divCnt;
        int restAmt = totalAmt % divCnt;
        List<SendingDetail> sdList = new ArrayList<>();

        for (int i = 1; i <= divCnt; i++) {
            SendingDetail sd = SendingDetail.builder()
                    .sendingId(sending.getSendingId())
                    .order(i)
                    .receiveUserId(null)
                    .receiveAmt((i == 1 ? divAmt + restAmt : divAmt))
                    .receiveYn("N")
                    .receiveDtm(null)
                    .sending(sending)
                    .build();
            sdList.add(sd);
        }
        detailRepository.saveAll(sdList);
        log.info("sending_detail Table insert SUCCESS | 총 {}건", divCnt);
    }

    @Override
    public ResponseTakeDto updateReceivedSendingDetail(Long userId, Sending sending) throws Exception {
        ResponseTakeDto responseTakeDto;
        SendingDetail takeMoneyDetail = detailRepository.findFirstBySendingIdAndReceiveYnOrderByOrderAsc(sending.getSendingId(), "N");
        if (takeMoneyDetail == null) {
            throw new TakeMoneyWarning("이미 모두 분배된 뿌리기입니다.");
        }
        takeMoneyDetail = SendingDetail.builder()
                .sendingId(sending.getSendingId())
                .order(takeMoneyDetail.getOrder())
                .receiveAmt(takeMoneyDetail.getReceiveAmt())
                .receiveDtm(LocalDateTime.now())
                .receiveUserId(userId)
                .receiveYn("Y")
                .sending(sending)
                .build();
        detailRepository.save(takeMoneyDetail);
        log.info("sending_detail Table update SUCCESS | {}", takeMoneyDetail);

        // 응답 DTO 생성 후 리턴
        responseTakeDto = ResponseTakeDto.builder()
                .receiveAmt(takeMoneyDetail.getReceiveAmt())
                .responseCd(200)
                .message("뿌리기 받기 성공")
                .build();
        log.debug("responseTakeDto 생성 성공 | {}", responseTakeDto.toString());
        return responseTakeDto;
    }

    @Override
    public ResponseSearchDto findDetailBySending(Sending sending) throws Exception {
        ResponseSearchDto responseSearchDto;

        // 1. 받기 완료된 금액 조회
        int totRecvAmt = detailRepository.getSumReceiveAmtBySendingId(sending.getSendingId());
        log.debug("sending_detail 조회 성공 | 받기 완료된 금액 {}", totRecvAmt);

        // 2. 리턴에 필요한 받은 금액, 받은 유저ID 만 List에 저장
        List<TakeFinishedInfoDto> receiveFinishedList =
                detailRepository.findSendingDetailsBySendingIdOrderByOrderAsc(sending.getSendingId());

        // 3. response Dto에 넣어 리턴
        responseSearchDto = ResponseSearchDto.builder()
                .responseCd(200)
                .message("뿌리기 조회 성공")
                .sendingTime(sending.getRgtDtm())
                .totalAmt(sending.getTotalAmt())
                .totalReceivedAmt(totRecvAmt)
                .receiveFinishedInfoList(receiveFinishedList)
                .build();
        log.debug("responseSearchDto 생성 성공 | {}", responseSearchDto.toString());
        return responseSearchDto;
    }
}
