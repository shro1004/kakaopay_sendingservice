package com.kyk.preprob.kakaopay.demo.entity;

import com.kyk.preprob.kakaopay.demo.dto.TakeFinishedInfoDto;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;

@Getter
@Setter
@ToString
@Entity
@Builder
@SqlResultSetMappings({
        @SqlResultSetMapping(
                name = "SearchResultMapping",
                classes = {
                        @ConstructorResult(
                                targetClass = TakeFinishedInfoDto.class,
                                columns = {
                                        @ColumnResult(name = "receive_amt", type = Integer.class),
                                        @ColumnResult(name = "receive_user_id", type = Long.class)
                                }
                        )
                }
        )
})
@NamedNativeQueries({
        @NamedNativeQuery(name = "SendingDetail.GetReceiveAmtAndReceiveUserId", query = "" +
                "select sd.receive_amt,         " +
                "       sd.receive_user_id      " +
                "  from sending_detail sd       " +
                " where sd.sending_id = ?1      " +
                "   and sd.receive_yn = 'Y'     " +
                " order by sd.s_order asc       "
                , resultSetMapping = "SearchResultMapping")
})
@Table(name = "sending_detail")
@IdClass(SendingDetailId.class)
@NoArgsConstructor
public class SendingDetail {
    @Id
    @Column(name = "sending_id", nullable = false)
    private Long sendingId;

//    @Id
//    @Column(name = "user_id", nullable = false)
//    private Long userId;
//
//    @Id
//    @Column(name = "room_id", nullable = false)
//    private String roomId;

    @Id
    @Column(name = "s_order", nullable = false)
    private int order;

    @Column(name = "receive_user_id")
    private Long receiveUserId;

    @Column(name = "receive_amt")
    private int receiveAmt;

    @Column(name = "receive_yn")
    private String receiveYn;

    @Column(name = "receive_dtm")
    private LocalDateTime receiveDtm;

    @ManyToOne(fetch = FetchType.LAZY, optional = true)
    /** 연관관계만 맺는 역할만 하고 실제 값은 @id 컬럼을 실제 매핑에 사용 */
    @JoinColumns(value = {
            @JoinColumn(name = "sending_id", updatable = false, insertable = false)
//           ,@JoinColumn(name = "user_id", updatable = false, insertable = false)
//           ,@JoinColumn(name = "room_id", updatable = false, insertable = false)
    }, foreignKey = @ForeignKey(value = ConstraintMode.NO_CONSTRAINT))
    private Sending sending;

    public SendingDetail(Long sendingId, int order, Long receiveUserId, int receiveAmt,
                         String receiveYn, LocalDateTime receiveDtm, Sending sending) {
        this.sendingId = sendingId;
//        this.userId = userId;
//        this.roomId = roomId;
        this.order = order;
        this.receiveUserId = receiveUserId;
        this.receiveAmt = receiveAmt;
        this.receiveYn = receiveYn;
        this.receiveDtm = receiveDtm;
        this.sending = sending;
    }
}
