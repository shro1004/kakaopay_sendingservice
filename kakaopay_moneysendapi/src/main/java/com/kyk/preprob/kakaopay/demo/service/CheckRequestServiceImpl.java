package com.kyk.preprob.kakaopay.demo.service;

import com.kyk.preprob.kakaopay.demo.dao.SendingDetailRepository;
import com.kyk.preprob.kakaopay.demo.dao.UserJoinRoomRepository;
import com.kyk.preprob.kakaopay.demo.entity.Sending;
import com.kyk.preprob.kakaopay.demo.entity.SendingDetail;
import com.kyk.preprob.kakaopay.demo.util.exception.SearchSendingException;
import com.kyk.preprob.kakaopay.demo.util.exception.TakeMoneyException;
import com.kyk.preprob.kakaopay.demo.util.exception.TakeMoneyWarning;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Slf4j
@Service("CheckRequestService")
public class CheckRequestServiceImpl implements CheckRequestService {
    @Autowired
    SendingDetailRepository detailRepository;
    @Autowired
    UserJoinRoomRepository joinRoomRepository;

    @Override
    public boolean checkTakeRequest(Long userId, String roomId, Sending sending) throws Exception {
        boolean flag = true;

        // 스스로 뿌린건 검증
        if (userId == sending.getUserId()) {
            throw new TakeMoneyException("스스로 뿌리기한 건은 받을 수 없습니다.");
        }
        // 동일한 대화방 확인
        if (!roomId.equals(sending.getRoomId())) {
            throw new TakeMoneyException("뿌리기가 호출된 대화방이 아닙니다.");
        }
        // 대화방에 참가한 유저인지
        if (joinRoomRepository.findByUserIdAndRoomIdAndAndJoinYn(userId, roomId, "Y") == null) {
            throw new TakeMoneyException("해당 뿌리기가 호출된 대화방에 속해있지 않습니다.");
        }
        // 해당 뿌리기를 받은 유저인지 검증
        SendingDetail checkMyReceive = detailRepository.findBySendingIdAndReceiveUserId(sending.getSendingId(), userId);
        if (checkMyReceive != null) {
            throw new TakeMoneyWarning("이미 한번 받은 뿌리기 건입니다.");
        }

        return flag;
    }

    @Override
    public boolean checkSearchRequest(Long userId, Sending sending) throws Exception {
        boolean flag = true;
        // 뿌린 사람만 조회할 수 가능하도록 검증
        if (userId != sending.getUserId()) {
            log.warn("요청한 ID : {} / 뿌리기 주인 ID : {}", userId, sending.getUserId());
            throw new SearchSendingException("뿌리기를 요청한 사람만 조회할 수 있습니다.");
        }
        return flag;
    }
}
