package com.kyk.preprob.kakaopay.demo.util.exception;

public class TakeMoneyException extends Exception {
    public TakeMoneyException(String msg) {
        super(msg);
    }
}
