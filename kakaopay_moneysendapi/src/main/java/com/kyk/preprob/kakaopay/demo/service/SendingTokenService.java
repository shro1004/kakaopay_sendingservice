package com.kyk.preprob.kakaopay.demo.service;

import com.kyk.preprob.kakaopay.demo.dto.RequestTokenDto;
import com.kyk.preprob.kakaopay.demo.dto.ResponseSendDto;
import com.kyk.preprob.kakaopay.demo.entity.Sending;

public interface SendingTokenService {
    String makeToken() throws Exception;

    ResponseSendDto insertSendingToken(String token, Sending sending) throws Exception;

    Sending findSendingByNotExpiredToken(RequestTokenDto requestTokenDto) throws Exception;

    Sending findSendingByBefore7DaysDtime(String token) throws Exception;
}
