package com.kyk.preprob.kakaopay.demo.controller;

import com.kyk.preprob.kakaopay.demo.dto.*;
import com.kyk.preprob.kakaopay.demo.entity.Sending;
import com.kyk.preprob.kakaopay.demo.service.*;
import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.*;

import java.util.Random;

@Slf4j
@RestController
public class ApiController {
    @Autowired
    private SendingSerivce sendingSerivce;
    @Autowired
    private SendingDetailService detailService;
    @Autowired
    private SendingTokenService tokenSerivce;
    @Autowired
    private CheckRequestService checkRequestService;
    @Autowired
    private ApplicationContext context;

    @GetMapping("/")
    public String ribbonPing() {
        int port = context.getBean(Environment.class).getProperty("server.port", Integer.class, 8444);
        log.debug("Access /");
        return port + " | heath check SUCCESS";
    }

    @PostMapping("/pay/sending/send")
    public ResponseSendDto sendMoney(@RequestHeader("X-USER-ID") Long userId,
                                     @RequestHeader("X-ROOM-ID") String roomId,
                                     @RequestBody RequestSendDto requestSendDto) throws Exception {
        log.info("Send Money 요청 받음");
        // 1. sending table insert
        Sending sending = sendingSerivce.insertSending(userId, roomId, requestSendDto);

        // 2. sending_detail table insert (분배작업)
        detailService.insertSendingDetails(sending);

        // 3. 토큰 생성
        String token = tokenSerivce.makeToken();

        // 4. sending_token table insert
        ResponseSendDto responseSendDto = tokenSerivce.insertSendingToken(token, sending);

        log.info("Send Money 응답 완료");
        return responseSendDto;
    }

    @PostMapping("/pay/sending/take")
    public ResponseTakeDto takeMoney(@RequestHeader("X-USER-ID") Long userId,
                                     @RequestHeader("X-ROOM-ID") String roomId,
                                     @RequestBody RequestTokenDto requestTokenDto) throws Exception {
        log.info("Take Money 요청 받음");
        // 1. 토큰으로 만료되지 않은 sending 을 조회한다.
        Sending sending = tokenSerivce.findSendingByNotExpiredToken(requestTokenDto);

        // 2. userId, roomId 검증 (스스로 뿌린기 건 take 불가, 동일한 대화방 확인, 대화방에 참가한 유저인지 확인)
        checkRequestService.checkTakeRequest(userId, roomId, sending);

        // 3. 아직 받지않은 sending_detail 1개를 가져와 할당 (update)
        ResponseTakeDto responseTakeDto = detailService.updateReceivedSendingDetail(userId, sending);

        log.info("Take Money 응답 완료");
        return responseTakeDto;
    }

    @GetMapping("/pay/sending/search")
    public ResponseSearchDto searchSending(@RequestHeader("X-USER-ID") Long userId,
                                           @RequestHeader("X-ROOM-ID") String roomId,
                                           @RequestParam(value = "token") String token) throws Exception {
        log.info("Search Sending 요청 받음");
        // 1. 7일 이내의 토큰으로 sending 을 조회한다.
        Sending sending = tokenSerivce.findSendingByBefore7DaysDtime(token);

        // 2. userId 검증 (뿌린 사람만 조회할 수 있습니다.)
        checkRequestService.checkSearchRequest(userId, sending);

        // 3. sending_detail 에서 뿌리기 상세 현황을 가져온다.
        ResponseSearchDto responseSearchDto = detailService.findDetailBySending(sending);
        log.info("Search Sending 응답 완료");
        return responseSearchDto;
    }

    /** 한글 램덤 생성 테스트*/
//    @GetMapping("/test")
//    public ResponseSendDto randomTokenTest() {
//        final int HANGEUL_BASE = 0xAC00;
//        Random random = new Random();
//
//        char test =(char) (HANGEUL_BASE + 21*28*random.nextInt(19) + 28*random.nextInt(21) + random.nextInt(28));
//        String token = Character.toString(test);
//        log.info("test Token making : {}", test);
//        return ResponseSendDto.builder()
//                .message("test")
//                .responseCd(200)
//                .token(token)
//                .build();
//    }
}
